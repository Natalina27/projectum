import {getUsers, setLoginUser} from "../../../api";
import {
  checkFieldsNotEmpty,
  emailValidation,
  getElById,
  getElBySel,
  removeValidation
} from "./validation";

const formLogin = getElBySel('.form--login');
const formRegistration = getElBySel('.form--registration');
const registrationLink = getElById('registration');

registrationLink.onclick = () => {
  removeValidation();
  formLogin.style.display = 'none';
  formRegistration.style.display = 'block';
};

formLogin.onsubmit = (e) => {
  e.preventDefault();
  const formLogin = getElBySel('.form--login');
  const logFields = formLogin.querySelectorAll('.gap--bottom');
  const notEmpty = checkFieldsNotEmpty(logFields);
  const formSetUp = getElBySel('.form--setup');
  const formRegistration = getElBySel('.form--registration');
  const emailLog = getElById('email');
  const passwordLog = getElById('pass');
  const emailValid = emailValidation(emailLog);

  if (notEmpty) {
    const users = getUsers();
    const user = users.filter(user => {
      return user.email === emailLog.value && user.password === passwordLog.value;
    })[0];
    if (emailValid) {
      if (user) {
        setLoginUser(user);
        formSetUp.style.display = 'block';
        formLogin.style.display = 'none';
        window.location.href = 'task-board.html';
      } else {
        alert('There  is no user with this email in the system! You need to register, please');
        formLogin.style.display = 'none';
        formRegistration.style.display = 'block';
        removeValidation();
      }
      emailLog.value = '';
      users.value = '';
    }
  }
};
