import {getUsers, setLoginUser, setUser, updateUser} from "../../../api";
import {
  checkFieldsNotEmpty,
  emailValidation,
  getElById,
  getElBySel, nameValidation, passValidation,
  removeValidation
} from "./validation";

const user = {};

const formRegistration = getElBySel('.form--registration');
const formLogin = getElBySel('.form--login');
const formSetUp = getElBySel('.form--setup');

const loginLink = getElById('login');
const emailReg = getElById('email-reg');
const passwordReg = getElById('pass-reg');
const userReg = getElById('user');

const regFields = formRegistration.querySelectorAll('.gap--bottom');

loginLink.onclick = () => {
  formLogin.style.display = 'block';
  formRegistration.style.display = 'none';
  removeValidation();
};

formSetUp.onsubmit = (e) => {
  e.preventDefault();
  const radioBtn = formSetUp.querySelector('input:checked');
  user.team = radioBtn.id;
  updateUser(user);
  setLoginUser(user);
  window.location.href = 'task-board.html';
};

formRegistration.onsubmit = (e) => {
  removeValidation();
  e.preventDefault();

  const users = getUsers('credentials');

  const notEmpty = checkFieldsNotEmpty(regFields);
  const emailValid = emailValidation(emailReg);
  const nameValid = nameValidation(userReg);
  const passwordValid = passValidation(passwordReg);

  if (notEmpty && emailValid && nameValid && passwordValid) {
    if (users.some(user => user.email === emailReg.value)) {
      alert('The user is already exist!');
    } else {
      formSetUp.style.display = 'block';
      formRegistration.style.display = 'none';
      user.email = emailReg.value;
      user.name = userReg.value;
      user.password = passwordReg.value;

      setUser(user);
    }
  }
};


