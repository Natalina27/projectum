//General functions
export const getElById = (id) => document.getElementById(id);
export const getElBySel = (selector) => document.querySelector(selector);

//Validation functions
export const generateError = (el, text) => {
  const error = document.createElement('div');
  el.className = 'error-reg';
  error.style.color = 'red';
  error.id = 'errorText';
  error.innerHTML = text;
  return error;
};
export const checkFieldsNotEmpty = (fields) => {
  let response = true;
  fields.forEach(item => {
    if (!item.value) {
      const el = generateError(item, ' Cannot be blank');
      item.parentElement.insertBefore(el, item);
      response = false;
    }
  });
  return response;
};
export const emailValidation = (email) => {
  return  email.value.includes('@');
};
export const nameValidation = (name) => {
  let validName = name.value.split(' ');
  if (validName.length === 2 && validName.every(el => el.length >= 3)) {
    return validName;
  } else {
    validName = generateError(name,'Name is not valid!');
    name.parentElement.insertBefore(validName, name);
  }
};
export const passValidation = (pass) => {
  if (pass.value.length >= 8) {
    return pass;
  } else {
    const password = generateError(pass, 'Password need to be min 8 symbols!');
    pass.parentElement.insertBefore(password, pass);
  }
};
export const removeValidation = () => {
  const errors = document.querySelectorAll('.error-reg');
  const errorText = document.querySelectorAll('#errorText');
  errors.forEach(item => {
    item.classList.remove('error-reg');
    item.classList.add('gap--bottom');
  });
  errorText.forEach(item => item.remove());
};


