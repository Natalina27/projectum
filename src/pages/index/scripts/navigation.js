import {getElById} from "./validation";

export const calendarNav = () => {
  const calendarNav = getElById('calendar');
  calendarNav.onclick = () => {
    window.location.href = 'calendar.html';
  };
};

export const kanbanNav = () => {
  const kanbanNav = getElById('kanban');
  kanbanNav.onclick = () => {
    window.location.href = 'task-description.html';
  };
};

export const tasksNav = () => {
  const tasksNav = getElById('tasks');
  tasksNav.onclick = () => {
    window.location.href = 'task-board.html';
  };
};
