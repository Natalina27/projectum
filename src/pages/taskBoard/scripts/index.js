import '../../../css/style.scss';
import {generateTaskCard, getUsers, renderTasks, setTask} from "../../../api";
import {getElById, getElBySel} from "../../index/scripts/validation";
import {calendarNav, kanbanNav} from "../../index/scripts/navigation";

const task ={};

const main = getElBySel('main');
const formTask = getElBySel('.form--task');
const kanbanBlock = getElBySel('.kanban__block');
const tasksColumns = document.querySelectorAll('.task__column');

const backlogColumn = getElById('backlog');
const todoColumn = getElById('todo');
const doneColumn = getElById('done');

const taskTitle = getElById('taskTitle');
const taskExecutor = getElById('executor');
const taskDeadline = getElById('deadline');
const taskDescription = getElById('taskDescription');
const taskTag = getElBySel('.taskTag');

kanbanBlock.onclick = (e) => {
const addTask = e.target;
const taskParent = addTask.parentNode.parentNode;

switch (taskParent.id){
  case 'backlog':
    backlogColumn.classList.add('active');
    todoColumn.classList.remove('active');
    doneColumn.classList.remove('active');
    break;
  case 'todo':
    todoColumn.classList.add('active');
    backlogColumn.classList.remove('active');
    doneColumn.classList.remove('active');
    break;
  case 'done':
    doneColumn.classList.add('active');
    backlogColumn.classList.remove('active');
    todoColumn.classList.remove('active');
    break;
}

if(addTask){
  main.className = 'task__setup';
  formTask.style.display = 'block';
  tasksColumns.forEach(item => item.style.display = 'none');
}
};

formTask.onsubmit = (e) => {
  e.preventDefault();
  main.className = 'flex kanban__block';
  formTask.style.display = 'none';
  tasksColumns.forEach(item => item.style.display = 'block');
  const active = kanbanBlock.querySelector('.active');

  task.title = taskTitle.value;
  task.executor = taskExecutor.value;
  task.deadline = taskDeadline.value;
  task.description = taskDescription.value;
  task.board = active.id;
  task.tag = taskTag.value;

  setTask(task);
  generateTaskCard(task);
};

function generateAssigneeList() {
  const users = getUsers();
  const select = getElBySel('[data-select=assignee]');
  users.forEach((user) => {
    const value = document.createElement('option');
    value.value = user.email;
    value.innerHTML = user.name;
    select.appendChild(value);
  });
}
generateAssigneeList();
kanbanNav();
calendarNav();
renderTasks();



