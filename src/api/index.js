// логика работы с локальным хранилищем

//users
import {getElById} from "../pages/index/scripts/validation";

export const getUsers = () => {
  const users = localStorage.getItem('credentials');
  if(!users){
    return [];
  }else{
    return JSON.parse(users);
  }
};

export const setUser = (obj) => {
    const users = getUsers('credentials');
    localStorage.setItem('credentials', JSON.stringify([...users, obj]));
};

export const updateUser = (obj) => {
  const users = JSON.parse(localStorage.getItem('credentials'));

  const newUsers = users.map(user => {
    if(user.email === obj.email) {
      return obj;
    } else {
      return user;
    }
  });
  localStorage.setItem('credentials', JSON.stringify(newUsers));
};

export const setLoginUser = (obj) => {
  localStorage.setItem('login', JSON.stringify(obj));
};
export const getLoginUser = () => {
  const user = localStorage.getItem('login');
  if(!user){
    return {};
  }else{
    return JSON.parse(user);
  }
};

//tasks
export const  generateTaskCard = (task) => {
  const parent = getElById(task.board);

  const a = document.createElement('a');
  a.classList.add('task');
  a.classList.add('task__block');

  const newTask = parent.appendChild(a);
  const descrParag = document.createElement('p');
  descrParag.classList.add('task__description');
  descrParag.innerText = task.title;
  newTask.appendChild(descrParag);

  const div = document.createElement('div');
  div.classList.add('flex');

  const users = getUsers();
  const assignee = users.find((user) => user.email === task.executor);
  // console.log('assignee', assignee);

  // div.innerText = assignee.name;
  let team = '';
  switch (assignee.team){
    case 'marketing':
      team = 'marketing';
      break;
    case 'finance':
      team = 'finance';
      break;
    case 'backend':
      team = 'development';
      break;
    case 'sales':
      team = 'marketing';
      break;
    case 'design':
      team = 'design';
      break;
    case 'frontend':
      team = 'development';
      break;
  }

  const executor = newTask.appendChild(div);
  const img = document.createElement('img');
  img.classList.add('task__executor');
  img.src = 'img/userpic.jpg';
  const execParag = document.createElement('p');
  execParag.classList.add('task__label');
  execParag.classList.add(team);
  execParag.innerText = assignee.team;
  executor.appendChild(img);
  executor.appendChild(execParag);



};

export const getTasks = () => {
  const tasks = localStorage.getItem('tasks');
  if(!tasks){
    return [];
  }else{
    return JSON.parse(tasks);
  }
};

export const setTask = (obj) => {
  const tasks = getTasks('tasks');
  localStorage.setItem('tasks', JSON.stringify([...tasks, obj]));
};

export const renderTasks = () => {
  const tasks = getTasks();
  // console.log('tasks', tasks);
  tasks.forEach(task => {
    document.addEventListener("DOMContentLoaded", () => {
      generateTaskCard(task);
    });
  });
};
